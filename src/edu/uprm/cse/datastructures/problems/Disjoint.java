package edu.uprm.cse.datastructures.problems;

import javax.naming.spi.DirStateFactory.Result;

import edu.uprm.cse.datastructures.set.ArraySet;
import edu.uprm.cse.datastructures.set.Set;

public class Disjoint {

	// ADD YOUR CODE HERE

	public static void main(String[] args) {
		Set<Integer> S1 = new ArraySet<Integer>();
		Set<Integer> S2 = new ArraySet<Integer>();
		Set<Integer> S3 = new ArraySet<Integer>();
		Set<Integer> S4 = new ArraySet<Integer>();

		S1.add(1);
		S1.add(2);

		S2.add(0);

		S3.add(5);
		S3.add(8);
		S3.add(9);

		S4.add(5);

		@SuppressWarnings("unchecked")
		Object[] A1 = new Object[3];
		A1[0] = S1;
		A1[1] = S2;
		A1[2] = S3;

		System.out.println("S1, S2, and S3 disjoint: " + checkDisjoint(A1));
		Object[] A2 = new Object[4];
		A2[0] = S1;
		A2[1] = S2;
		A2[2] = S3;
		A2[3] = S4;

		System.out.println("S1, S2, S3, and S4 disjoint: " + checkDisjoint(A2));

	}

	private static <E> boolean checkDisjoint(Object[] a) {
//This one checks disjointment of each element in the array
//		int count = 0;
//		for (int i = 0; i < a.length; i++) {
//			for (int j = 0; j < a.length; j++) {
//				if (i != j) {
//					if (!(((Set) a[i]).intersection(((Set) a[j])).isEmpty())) {
//						count++;
//					}
//				}
//			}
//		}
//		if (count > 1) {
//			return false;
//		}
//		return true;
		Set<Object> result = new ArraySet<Object>();
		result = (Set<Object>) a[0];
		for (int i = 1; i < a.length; i++) {
			result = ((Set) a[i]).intersection(result);
		}
		return result.isEmpty();
	}
}
